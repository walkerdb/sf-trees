import React from 'react'
import { getTreeLink } from './treeHelpers';

const getBackgroundColorStyling = (color) => {
  switch (color) {
    case 'blue': return { background: '#0083D1' }
    case 'pink': return { background: '#E300CB' }
    case 'purple': return { background: '#7D4BFD' }
    case 'green': return { background: '#00B400' }
    case 'yellow': return { background: '#7A7C00' }
    case 'rainbow-2': return { background: "linear-gradient(217deg, rgba(255,0,0,.8), rgba(255,0,0,0) 70.71%),\n" +
        "            linear-gradient(127deg, rgba(0,255,0,.8), rgba(0,255,0,0) 70.71%),\n" +
        "            linear-gradient(336deg, rgba(0,0,255,.8), rgba(0,0,255,0) 70.71%)" }
    default: return { background: color }
  }
}

const ColorBlock = ({ color }) => (
  <div style={{
    display: 'inline-block',
    height: 10,
    width: 10,
    marginRight: 5,
    ...getBackgroundColorStyling(color),
  }} />
)

export const Legend = ({
  onLocationClick,
  treeSummaries,
  setHoveredTree,
  isDetailsOpen,
  setIsDetailsOpen
}) => {
  return (
    <div style={{ width: 250, padding: 10 }} onClick={e => e.stopPropagation()}>
      <div style={{ paddingBottom: 10 }}>
        Click <a href="#" onClick={onLocationClick}>here</a> to center on your location, or just click anywhere on the map to see the trees near there
      </div>
      <details open={isDetailsOpen}>
        <summary onClick={() => setIsDetailsOpen(!isDetailsOpen)}>Tree summary ({Object.keys(treeSummaries).length} species in view)</summary>
        {Object.entries(treeSummaries)
          .sort((a, b) => a[1].count > b[1].count ? -1 : 1)
          .map(([treeName, details]) => {
            return (
              <div
                onMouseOver={() => setHoveredTree(treeName)}
                onMouseOut={() => setHoveredTree(null)}
              >
                <ColorBlock color={details.color} />
                <a href={getTreeLink(details.tree.scientificName)} target="_blank">{treeName}</a>: {details.count}
              </div>
            )
          })}
      </details>
    </div>
  )
}
