import { differenceInMonths } from 'date-fns';

export const DEFAULT_ZOOM = 19

const colors = ['blue', 'red', 'yellow', 'pink', 'purple', 'green']

export const fetchTrees = (location, radius, setTrees, setTreeSummary) => {
  fetch(`https://walker.is/excited/about/trees/backend?lat=${location[0]}&lon=${location[1]}&radius=${radius}`)
    .then(result => result.json())
    .then(result => {
      const availableColors = [...colors]
      const treeSummary = result.reduce((reduced, tree) => {
        const treeName = getTreeName(tree);
        reduced[treeName] = reduced[treeName] || { tree: tree, count: 0 }
        reduced[treeName].count += 1
        return reduced
      }, {});

      Object.entries(treeSummary)
        .sort((a, b) => a[1].count > b[1].count ? -1 : 1)
        .forEach(([treeName, _]) => {
          treeSummary[treeName].color = availableColors.pop() || "rainbow-2"
        })

      setTreeSummary(treeSummary)
      setTrees(result)
    })
}

const dateRegex = /(\d\d\d\d)-(\d\d?)-(\d\d?)/;

export const extractDate = (date) => {
  if (!date) {
    return ''
  }

  const matches = dateRegex.exec(date)

  if (!matches || matches.length < 4) {
    return ''
  }

  const [_, year, month, day] = matches

  return `${year}-${month.padStart(2, '0')}-${day.padStart(2, '0')}`
}

export const getAge = (treePlantedAt) => {
  const now = new Date();
  const monthsSincePlanted = differenceInMonths(now, new Date(treePlantedAt))

  return `${Math.floor(monthsSincePlanted / 12)} years, ${monthsSincePlanted % 12} months`
}

export const getTreeLink = (scientificName) => {
  if (!scientificName) {
    return ''
  }
  const nameForUrl = scientificName
    .toLowerCase()
    .replace(/[^a-z _\-]/g, '')
    .replace(/ /g, '-')
    .replace(/^tristania-conferta$/, 'lophostemon-confertus');
  return `https://selectree.calpoly.edu/tree-detail/${nameForUrl}`
}

export function getTreeName(tree) {
  return tree.commonName || tree.scientificName || "Unknown Tree";
}

export const DEFAULT_ICON_SETTINGS = {
  iconUrl: "marker-blue.png",
  shadowUrl: 'https://unpkg.com/leaflet@1.7.1/dist/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
}
