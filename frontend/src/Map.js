import React, { useState, useEffect } from 'react'
import Leaflet from 'leaflet';
import { MapContainer, Marker, Popup, TileLayer, useMap, useMapEvent } from 'react-leaflet'
import * as ReactDOM from 'react-dom';
import { Legend } from './Legend';
import { DEFAULT_ICON_SETTINGS, DEFAULT_ZOOM, extractDate, fetchTrees, getAge, getTreeLink, getTreeName } from './treeHelpers';

// these are unfortunately globally stateful due to wonky interactions with the Leaflet lib
let lastLocationMarker;
let lastLocationCircle;

const TreeMap = () => {
  const [radius, setRadius] = useState(100)
  const [trees, setTrees] = useState([])
  const [treeSummary, setTreeSummary] = useState({})
  const [hoveredTree, setHoveredTree] = useState(null)
  const [lat, setLat] = useState(37.783131)
  const [lon, setLon] = useState(-122.434215)
  const [isDetailsOpen, setIsDetailsOpen] = useState(false)
  useEffect(() => {
    const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    setIsDetailsOpen(!isMobile)
  }, [])

  useEffect(() => {
    fetchTrees([lat, lon], radius, setTrees, setTreeSummary)
  }, [lat, lon, radius, setTrees, setTreeSummary])

  useMapEvent('locationfound', (e) => {
    console.log('location found!')
    setLat(e.latlng.lat)
    setLon(e.latlng.lng)
    const radius = e.accuracy;
    lastLocationMarker && map.removeLayer(lastLocationMarker)
    lastLocationMarker = Leaflet.marker(e.latlng, { icon: Leaflet.icon({...DEFAULT_ICON_SETTINGS, iconUrl: 'https://walker.is/excited/about/trees/marker-rainbow-2.png'}) });
    lastLocationMarker.addTo(map)
      .bindPopup("You are within " + Math.round(radius) + " meters from this point")

    lastLocationCircle && map.removeLayer(lastLocationCircle)
    lastLocationCircle = Leaflet.circle(e.latlng, radius);
    lastLocationCircle.addTo(map)

    map.flyTo(e.latlng, DEFAULT_ZOOM)
  })

  useMapEvent('click', (e) => {
    if(e.originalEvent.target.localName === 'button' || e.originalEvent.target.localName === 'summary') {
      console.log('in click, but skipping')
      return;
    }
    map.flyTo(e.latlng, DEFAULT_ZOOM)
    fetchTrees([e.latlng.lat, e.latlng.lng], radius, setTrees, setTreeSummary)
  })

  const map = useMap();

  const onLocationClick = (e) => {
    e.stopPropagation()
    map.locate()
  }

  useEffect(() => {
    if (!map) return;

    const legend = Leaflet.control({ position: "topright" });

    legend.onAdd = () => {
      const div = Leaflet.DomUtil.create("div", "info legend");

      ReactDOM.render(
        <Legend
          onLocationClick={onLocationClick}
          treeSummaries={treeSummary}
          setHoveredTree={setHoveredTree}
          isDetailsOpen={isDetailsOpen}
          setIsDetailsOpen={setIsDetailsOpen}
        />,
        div
      )

      return div;
    };

    legend.addTo(map);

    return () => legend.remove()
  }, [map, trees, setHoveredTree])

  const treeMarkers =  trees.map(tree => {
    const plantedAt = extractDate(tree.plantedAt)
    const treeLink = getTreeLink(tree.scientificName)
    const treeName = getTreeName(tree)
    if (hoveredTree && hoveredTree !== treeName) {
      return null
    }
    return (
      <Marker
        position={[Number(tree.location.coordinates[1]), Number(tree.location.coordinates[0])]}
        key={tree._id}
        icon={Leaflet.icon({...DEFAULT_ICON_SETTINGS, iconUrl: `https://walker.is/excited/about/trees/marker-${treeSummary[treeName]?.color || 'rainbow-2'}.png`})}
      >
        <Popup>
          <h2>{treeLink ? <a href={treeLink} target="_blank">{treeName}</a> : treeName}</h2>
          <ul>
            <li><strong>Scientific name:</strong> <em>{tree.scientificName}</em></li>
            {plantedAt && <li><strong>Age:</strong> {getAge(plantedAt)}</li>}
            <li><strong>Address:</strong> {tree.address}</li>
            <li><strong>Site details:</strong> {tree.siteInfo}</li>
          </ul>
        </Popup>
      </Marker>
    )
  })

  return <div>{treeMarkers}</div>
}

export const Root = () => (
  <MapContainer center={[37.783131, -122.434215]} zoom={DEFAULT_ZOOM} maxZoom={30}>
    <TileLayer
      attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      url='https://{s}.basemaps.cartocdn.com/rastertiles/light_all/{z}/{x}/{y}.png'
      maxZoom={30}
    />
    <TreeMap />
  </MapContainer>
)
