const express = require('express')
const MongoClient = require('mongodb').MongoClient;
const cors = require('cors');
const fs = require('fs');

const app = express()
app.use(cors());

const mongoUrl = `mongodb+srv://trees:${process.env.TREES_MONGO_PASSWORD}@cluster0.vfyrt.mongodb.net/trees?retryWrites=true&w=majority`;

const dbName = 'trees';
const client = new MongoClient(mongoUrl);

// const dbData = JSON.parse(fs.readFileSync('./jsonified_trees.json'))
//
// client.connect(async (err) => {
//   console.log('Connected successfully to server');
//
//   const db = client.db(dbName);
//   console.log('dropping trees')
//   // await db.collection('trees').drop()
//
//   console.log('starting new trees import')
//   await db.collection('trees').insertMany(dbData)
//
//   console.log('finished trees import')
//
//   console.log('starting index')
//   await db.collection('trees').createIndex({ location: '2dsphere' })
//
//   console.log('finished index')
// });



app.get('/', function (req, res) {
  const latitude = Number(req.query.lat);
  const longitude = Number(req.query.lon);
  const radius = Number(req.query.radius || 100);

  client.connect((err) => {
    if (err) {
      console.error(err)
      console.log(mongoUrl)
      return
    }
    const db = client.db(dbName);
    const cursor = db.collection('trees').find({
      location: { $nearSphere: { $geometry: { type: "Point", coordinates: [ longitude, latitude ] }, $maxDistance: radius } }
    })

    cursor.count().then(count => console.log(`found ${count} trees`))

    cursor.toArray().then((result) => {
      res.send(result)
    })
  });

})

app.listen(7788, () => {
  console.log('listening on port 7788!')
})
