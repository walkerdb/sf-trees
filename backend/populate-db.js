const MongoClient = require('mongodb').MongoClient;
const fs = require('fs');

const mongoUrl = `mongodb+srv://trees:${process.env.TREES_MONGO_PASSWORD}@cluster0.vfyrt.mongodb.net/trees?retryWrites=true&w=majority`;

const dbName = 'trees';
const client = new MongoClient(mongoUrl);

const dbData = JSON.parse(fs.readFileSync('./jsonified_trees.json'))

client.connect(async (err) => {
  console.log('Connected successfully to server');

  const db = client.db(dbName);
  console.log('dropping trees')
  // await db.collection('trees').drop()

  console.log('starting new trees import')
  await db.collection('trees').insertMany(dbData)

  console.log('finished trees import')

  console.log('starting index')
  await db.collection('trees').createIndex({ location: '2dsphere' })

  console.log('finished index')
});
